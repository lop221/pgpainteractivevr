﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class LocomotionController : MonoBehaviour
{
    public XRController leftTeleportRay;
    public InputHelpers.Button teleprtButton;
    public float treshold = 0.1f;

    public bool enableTeleport { get; set;} = true;

    // Update is called once per frame
    void Update()
    {
        if (leftTeleportRay) 
        {
            leftTeleportRay.gameObject.SetActive(enableTeleport && CheckIfActivated(leftTeleportRay));
        }
    }

    public bool CheckIfActivated(XRController cont) {
        InputHelpers.IsPressed(cont.inputDevice, teleprtButton, out bool isActivated, treshold);
        return isActivated;
    }
}
