﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandControl : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject handModel;
    public InputDeviceCharacteristics controllerCharacteristic;


    private Animator handAnimator;
    private GameObject activeModel;
    private InputDevice controller;
    private List<InputDevice> devices = new List<InputDevice>();
    void Start()
    {
        activeModel = GameObject.Instantiate(handModel, this.transform);
        handAnimator = activeModel.GetComponent<Animator>();
        UpdateDevice();
    }

    void UpdateDevice() {
        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristic, devices);
        if (devices.Count > 0) {
            controller = devices[0];
        }
    }

    void UpdateAnimation() {
        if (controller.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            handAnimator.SetFloat("Trigger", triggerValue);
        }
        else {
            handAnimator.SetFloat("Trigger", 0);
        }

        if (controller.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            handAnimator.SetFloat("Grip", gripValue);
        }
        else
        {
            handAnimator.SetFloat("Grip", 0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (activeModel == null) //if hand isnt present try create it
        {
            activeModel = GameObject.Instantiate(handModel, this.transform);
            handAnimator = activeModel.GetComponent<Animator>();
           
        }

        if (!controller.isValid) {
            UpdateDevice();
        }

        UpdateAnimation();
    }
}
