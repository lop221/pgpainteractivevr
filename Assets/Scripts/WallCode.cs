﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCode : MonoBehaviour
{
    public bool lever0 { get; set; } = false;
    public bool lever1 { get; set; } = false;
    public bool lever2 { get; set; } = false;
    public bool lever3 { get; set; } = false;
    public bool lever4 { get; set; } = false;
    public bool lever5 { get; set; } = false;

    public GameObject wall;



    public IEnumerator Check(float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            if (!lever0 && lever1 && !lever2 && lever3 && !lever4 && lever5) //42
            {
                if (wall.activeSelf)
                {
                    wall.SetActive(false);
                }
            }
            else
            {
                if (!wall.activeSelf)
                {
                    wall.SetActive(true);
                }
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Check", 0.25);     
    }

   
}
