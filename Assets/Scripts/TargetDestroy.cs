﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        
        
        if (collision.gameObject.tag == "Target") {
            TargetScript ts = collision.gameObject.GetComponent<TargetScript>();
            GameObject tmp = GameObject.FindGameObjectWithTag("Score");
            ScoreScript sc = tmp.GetComponent<ScoreScript>();
            sc.score += ts.score;
            collision.gameObject.SetActive(false);
        }
        Destroy(this.gameObject);
    }
}
