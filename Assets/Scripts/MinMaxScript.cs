﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MinMaxScript : MonoBehaviour
{
    public float treshold = 1f;
    public enum HingeJointState { Min, Max, None }
    public HingeJointState hingeJointState = HingeJointState.None;
    private HingeJoint hinge;
    public UnityEvent OnMinLimitReached;
    public UnityEvent OnMaxLimitReached;
    public UnityEvent OnNoneLimitReached;
    // Start is called before the first frame update
    void Start()
    {
        hinge = GetComponent<HingeJoint>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float minimumAngle = Mathf.Abs(hinge.angle - hinge.limits.min);
        float maximumAngle = Mathf.Abs(hinge.angle - hinge.limits.max);
        if (minimumAngle < treshold) 
        {
            if (hingeJointState != HingeJointState.Min)
                OnMinLimitReached.Invoke();

            hingeJointState = HingeJointState.Min;
        }
        else if (maximumAngle < treshold)
        {
            if (hingeJointState != HingeJointState.Max)
                OnMaxLimitReached.Invoke();

            hingeJointState = HingeJointState.Max;
        }
        else //No Limit reached
        {
           
            if (hingeJointState != HingeJointState.None)
                OnNoneLimitReached.Invoke();

            hingeJointState = HingeJointState.None;
        }




    }
}
