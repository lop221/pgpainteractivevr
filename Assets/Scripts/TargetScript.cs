﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{
    public bool moveable { get; set; } = false;
    public bool scalable { get; set; } = false;
    public float score { get; set; } = 1f;

    public float speedz = 5f;
    public float speedy = 5f;

    public Vector3 minposition, maxposition;

    public float scalespeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (moveable) {
            this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + (speedy* Time.deltaTime), this.gameObject.transform.position.z +(speedz * Time.deltaTime));

            if (this.gameObject.transform.position.z >= maxposition.z || (this.gameObject.transform.position.z <= minposition.z)) {
                speedz *= -1;
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + (speedy * Time.deltaTime), this.gameObject.transform.position.z + (speedz * Time.deltaTime));
            }

            if (this.gameObject.transform.position.y >= maxposition.y || (this.gameObject.transform.position.y <= minposition.y))
            {
                speedy *= -1;
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + (speedy * Time.deltaTime), this.gameObject.transform.position.z + (speedz * Time.deltaTime));
            }
        }

        if (scalable) {
            this.gameObject.transform.localScale = new Vector3(this.transform.localScale.x - (scalespeed * Time.deltaTime*50), this.transform.localScale.y - (scalespeed * Time.deltaTime * 50), this.transform.localScale.z - (scalespeed * Time.deltaTime * 50));
            if (this.gameObject.transform.localScale.x <= 0.1f || this.gameObject.transform.localScale.x >= 1f)
            {
                scalespeed *= -1;
            }
        }
    }
}
