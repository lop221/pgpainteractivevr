﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class GenerateTargets : MonoBehaviour
{
    public float minY = 0.5f, maxY = 3;
    public int numOfTargets=10;
    public int level1NumTargets = 10;
    public int level2NumTargets = 10;
    public int level3NumTargets = 10;
    public int level4NumTargets = 10;
    public int level5NumTargets = 10;
    public GameObject redTarget;
    public GameObject greenTarget;
    public GameObject yellowTarget;
    public UnityEvent EndGame;
    public UnityEvent ResetGame;

    private int missionActive = 0;

    public TextMeshPro txt;
    private int oldSel;
    IEnumerator Spawn()
    {
        float xoffset, yoffset,zoffset;
        float maxxoffset = this.gameObject.transform.localScale.x*10 / 2;
        float maxzoffset = this.gameObject.transform.localScale.y*10 / 2;
        for (int i=0; i < numOfTargets; i++)
        {
            xoffset = Random.Range(-maxxoffset, maxxoffset);
            yoffset = Random.Range(minY,maxY);
            zoffset = Random.Range(-maxzoffset, maxzoffset);
            Instantiate(redTarget,new Vector3(this.transform.position.x + xoffset, this.transform.position.y+ yoffset, this.transform.position.z+zoffset), this.transform.rotation);
            yield return new WaitForSeconds(2);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        oldSel = missionActive;
        //StartCoroutine("Spawn");   
    }

    public void ChooseLevel(string name)
    {
        switch (name) {
            case "Level1":
                
                EndMission();
                missionActive = 1;
                Level01();
                break;
            case "Level2":
                
                EndMission();
                missionActive = 2;
                Level02();
                break;
            case "Level3":
                
                EndMission();
                missionActive = 3;
                Level03();
                break;
            case "Level4":
                
                EndMission();
                missionActive = 4;
                Level04();
                break;
            case "Level5":
                
                EndMission();
                missionActive = 5;
                Level05();
                break;
            default:
                break;

        }
       
    }

    void EndMission() {
        if (missionActive != 0) {
            //zapis high score
            GameObject[] listofTargets = GameObject.FindGameObjectsWithTag("Target");
            foreach (GameObject i in listofTargets) {
                Destroy(i);
            }
            EndGame.Invoke();
            StopAllCoroutines();
            missionActive = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (oldSel != missionActive) 
        {
            txt.text = missionActive.ToString();
            oldSel = missionActive;
        }
    }

    void Level01() {
        ResetGame.Invoke();
        StartCoroutine("Level01Generate");
    }
    void Level02()
    {
        ResetGame.Invoke();
        StartCoroutine("Level02Generate");
    }
    void Level03()
    {
        ResetGame.Invoke();
        StartCoroutine("Level03Generate");
    }
    void Level04()
    {
        ResetGame.Invoke();
        StartCoroutine("Level04Generate");
    }
    void Level05()
    {
        ResetGame.Invoke();
        StartCoroutine("Level05Generate");
    }


    IEnumerator Level05Generate()
    {
        float xoffset, yoffset, zoffset;
        float maxxoffset = this.gameObject.transform.localScale.x * 10 / 6;
        float maxzoffset = this.gameObject.transform.localScale.z * 10 / 2;
        for (int i = 0; i < level2NumTargets; i++)
        {
            xoffset = Random.Range(-maxxoffset, maxxoffset);
            yoffset = Random.Range(minY, maxY);
            zoffset = Random.Range(-maxzoffset, maxzoffset);
            int tmpi = Random.Range(0, 3);
            GameObject tmp;
            switch (tmpi)
            {
                case 0:
                    tmp = Instantiate(redTarget, new Vector3(this.transform.position.x - 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 1:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 2:
                    tmp = Instantiate(greenTarget, new Vector3(this.transform.position.x + 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                default:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
            }
            TargetScript ts = tmp.GetComponent<TargetScript>();
            ts.moveable = true;
            ts.maxposition = new Vector3(this.transform.position.x, this.transform.position.y + maxY, this.transform.position.z + maxzoffset);
            ts.minposition = new Vector3(this.transform.position.x, this.transform.position.y + minY, this.transform.position.z - maxzoffset);
            ts.speedy = 10f;
            ts.speedz = 10f;
            ts.scalable = true;
            ts.score = 50f;

            yield return new WaitForSeconds(5);
            Destroy(tmp);
        }
        EndMission();
    }

    IEnumerator Level04Generate()
    {
        float xoffset, yoffset, zoffset;
        float maxxoffset = this.gameObject.transform.localScale.x * 10 / 6;
        float maxzoffset = this.gameObject.transform.localScale.z * 10 / 2;
        for (int i = 0; i < level2NumTargets; i++)
        {
            xoffset = Random.Range(-maxxoffset, maxxoffset);
            yoffset = Random.Range(minY, maxY);
            zoffset = Random.Range(-maxzoffset, maxzoffset);
            int tmpi = Random.Range(0, 3);
            GameObject tmp;
            switch (tmpi)
            {
                case 0:
                    tmp = Instantiate(redTarget, new Vector3(this.transform.position.x - 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 1:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 2:
                    tmp = Instantiate(greenTarget, new Vector3(this.transform.position.x + 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                default:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
            }
            TargetScript ts = tmp.GetComponent<TargetScript>();
            ts.moveable = true;
            ts.maxposition = new Vector3(this.transform.position.x, this.transform.position.y + maxY, this.transform.position.z + maxzoffset);
            ts.minposition = new Vector3(this.transform.position.x, this.transform.position.y + minY, this.transform.position.z - maxzoffset);
            ts.speedy = 10f;
            ts.speedz = 10f;
            ts.score = 40f;
            yield return new WaitForSeconds(5);
            Destroy(tmp);
        }
        EndMission();
    }

    IEnumerator Level03Generate()
    {
        float xoffset, yoffset, zoffset;
        float maxxoffset = this.gameObject.transform.localScale.x * 10 / 6;
        float maxzoffset = this.gameObject.transform.localScale.z * 10 / 2;
        for (int i = 0; i < level2NumTargets; i++)
        {
            xoffset = Random.Range(-maxxoffset, maxxoffset);
            yoffset = Random.Range(minY, maxY);
            zoffset = Random.Range(-maxzoffset, maxzoffset);
            int tmpi = Random.Range(0, 3);
            GameObject tmp;
            switch (tmpi)
            {
                case 0:
                    tmp = Instantiate(redTarget, new Vector3(this.transform.position.x - 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 1:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 2:
                    tmp = Instantiate(greenTarget, new Vector3(this.transform.position.x + 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                default:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
            }
            TargetScript ts = tmp.GetComponent<TargetScript>();
            ts.maxposition = new Vector3(this.transform.position.x, this.transform.position.y + maxY, this.transform.position.z + maxzoffset);
            ts.minposition = new Vector3(this.transform.position.x, this.transform.position.y + minY, this.transform.position.z - maxzoffset);
            ts.moveable = true;
            ts.score = 20f;
            yield return new WaitForSeconds(5);
            Destroy(tmp);
        }
        EndMission();
    }

    IEnumerator Level02Generate()
    {
        float xoffset, yoffset, zoffset;
        float maxxoffset = this.gameObject.transform.localScale.x * 10 / 6;
        float maxzoffset = this.gameObject.transform.localScale.z * 10 / 2;
        for (int i = 0; i < level2NumTargets; i++)
        {
            xoffset = Random.Range(-maxxoffset, maxxoffset);
            yoffset = Random.Range(minY, maxY);
            zoffset = Random.Range(-maxzoffset, maxzoffset);
            int tmpi = Random.Range(0, 3);
            GameObject tmp;
            switch (tmpi) {
                case 0:
                    tmp = Instantiate(redTarget, new Vector3(this.transform.position.x - 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 1:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                case 2:
                    tmp = Instantiate(greenTarget, new Vector3(this.transform.position.x + 2 * maxxoffset + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
                default:
                    tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
                    break;
            }
            TargetScript ts = tmp.GetComponent<TargetScript>();
            ts.score = 5f;
            yield return new WaitForSeconds(5);
            Destroy(tmp);
        }
        EndMission();
    }

    IEnumerator Level01Generate()
    {
        float xoffset, yoffset, zoffset;
        float maxxoffset = this.gameObject.transform.localScale.x * 10 / 6;
        float maxzoffset = this.gameObject.transform.localScale.z * 10 / 2;
        for (int i = 0; i < level1NumTargets; i++)
        {
            xoffset = Random.Range(-maxxoffset, maxxoffset);
            yoffset = Random.Range(minY, maxY);
            zoffset = Random.Range(-maxzoffset, maxzoffset);
            GameObject tmp = Instantiate(yellowTarget, new Vector3(this.transform.position.x + xoffset, this.transform.position.y + yoffset, this.transform.position.z + zoffset), this.transform.rotation);
            TargetScript ts = tmp.GetComponent<TargetScript>();
            ts.score = 1f;
            yield return new WaitForSeconds(5);
            Destroy(tmp);
        }
        EndMission();
    }

}
