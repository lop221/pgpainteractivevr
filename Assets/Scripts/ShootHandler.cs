﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ShootHandler : MonoBehaviour
{
    public InputHelpers.Button shotBtn;
    public XRController controller;
    public float thresholdToFire = 0.1f;

    
    private SimpleShoot sshot;
    private bool once = true;

    
    // Start is called before the first frame update
    void Start()
    {
        sshot = this.gameObject.GetComponent<SimpleShoot>();
    }

    // Update is called once per frame
    void Update()
    {
        InputHelpers.IsPressed(controller.inputDevice, shotBtn, out bool isActivated, thresholdToFire);
        if (isActivated && once)
        {
            sshot.MyShoot();
            once = false;
        }

        if (!isActivated && !once)
        {
            once = true;
        }


    }

    
}
