﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class ScoreScript : MonoBehaviour
{

    public float score { get; set; } = 0f;
    private float oldscore;
    private TextMeshPro text;
    public TextMeshPro high;
    private float highScore = 0f;
    // Start is called before the first frame update
    void Start()
    {
        oldscore = score;
        text = GetComponent<TextMeshPro>();
    }
    public void WriteScore() 
    {
        if (score >= highScore) 
        {
            highScore = score;
            high.text = "High Score: \n" + score.ToString();
        }
        
    }
    public void ResetScore() {
        score = 0f;
        text.text = "Score: \n" + score.ToString();
        oldscore = score;
    }

    // Update is called once per frame
    void Update()
    {
        if (oldscore != score) {
            text.text = "Score: \n" + score.ToString();
            oldscore = score; 
        }
        
    }
}
