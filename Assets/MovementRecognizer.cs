﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using PDollarGestureRecognizer;
using System.IO;
using UnityEngine.Events;
public class MovementRecognizer : MonoBehaviour
{

    public XRNode inputSource;
    public InputHelpers.Button inputBtn;
    public float inputTreshold= 0.1f;
    public Transform movementSource;
    public float positionTreshold = 0.05f;
    public GameObject visualPrefab;
    public float timeToDestroy = 3.0f;
    public bool createGesture = false;
    public string gestureName;

    public float recogTreshold = 0.9f;

    [System.Serializable]
    public class UnityStringEvent : UnityEvent<string> { }
    public UnityStringEvent OnRecognized;

    private List<Gesture> gestureSet = new List<Gesture>();
    private bool isMoving = false;
    private List<Vector3> positionList = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        string[] gestureFiles = Directory.GetFiles(Application.persistentDataPath, "*.xml");
        foreach (var gesture in gestureFiles) 
        {
            Gesture tmp = GestureIO.ReadGestureFromFile(gesture);
            gestureSet.Add(tmp);
            Debug.Log(tmp.Name);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        InputHelpers.IsPressed(InputDevices.GetDeviceAtXRNode(inputSource), inputBtn, out bool isPressed, inputTreshold);

        if (!isMoving && isPressed) //start
        {
            StartMovement();
        }
        else if (isMoving && !isPressed)  //end
        {
            EndMovement();
        }
        else if (isMoving && isPressed) 
        {
            UpdateMovement();
        }

    }

    void StartMovement() 
    {
        isMoving = true;
        positionList.Clear();
        positionList.Add(movementSource.position);

        if (visualPrefab != null)
        {
            Destroy(Instantiate(visualPrefab, movementSource.position, Quaternion.identity), timeToDestroy);
        }
    }

    void UpdateMovement() 
    {
        
        Vector3 tmp = positionList[positionList.Count - 1];
        if (Vector3.Distance(tmp, movementSource.position) > positionTreshold) {
            
            positionList.Add(movementSource.position);
            if (visualPrefab != null)
            {
                Destroy(Instantiate(visualPrefab, movementSource.position, Quaternion.identity), timeToDestroy);
            }
        }
    }

    void EndMovement() 
    {
        isMoving = false;


        Point[] points = new Point[positionList.Count];
        for (int i = 0; i < positionList.Count; i++) {
            Vector2 point = Camera.main.WorldToScreenPoint(positionList[i]);
            points[i] = new Point(point.x, point.y, 0);
        }

        Gesture newGesture = new Gesture(points);
        if (createGesture)
        {
            newGesture.Name = gestureName;
            gestureSet.Add(newGesture);

            string name = Application.persistentDataPath + "/" + gestureName + ".xml";
            GestureIO.WriteGesture(points, gestureName, name);
            Debug.Log(name);
        }
        else
        {
            Result result = PointCloudRecognizer.Classify(newGesture, gestureSet.ToArray());
            Debug.Log(result.GestureClass + "  " + result.Score);
            if (result.Score > recogTreshold) 
            {
                OnRecognized.Invoke(result.GestureClass);
            
            }
        }

    }
}
