﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<GameObject> spawnObjects = new List<GameObject>();

    private List<GameObject> generated = new List<GameObject>();

    public void Spell(string name) {
        if (name == "X") {
            foreach (var i in generated) 
            {
                Destroy(i);
            }
            generated.Clear();
        }
        else
        {
            foreach (var i in spawnObjects)
            {
                if (i.name == name)
                {
                    GameObject tmp = Instantiate(i, this.transform);
                    generated.Add(tmp);
                }
            }
        }
    }
}
